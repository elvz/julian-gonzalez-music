/////////////////////////////////////////////////////////////////////////////////
// THE BELOW OBJECT AND ARRAY ARE CREATED TO ADD MORE FUNCTIONALITY AND A DYNAMIC APPROACH TO OUR WEBSITE INSTEAD OF ADDING ALL ELEMENTS TO THE HTML, THE BELOW CODE IS MORE UNIVERSAL.
/////////////////////////////////////////////////////////////////////////////////

const thumbnailsObject = {
    hassan: {
      name: "Andy",
      age: "Age 6",
      review:
        "Julian’s lessons are always fun!",
      thumbnailSrc: "./images/image.jpg",
      avatarSrc: "./images/image.jpg",
      alt: "Andy",
    },
  
    john: {
      name: "Kate",
      age: "Age 10",
      review:
        "Julian is a great Double Bass teacher. He explains things properly and shows me how to do things the right way. He is a kind teacher and has a lot of patience. I look forward to my lessons every week!",
      thumbnailSrc: "./images/image.jpg",
      avatarSrc: "./images/image.jpg",
      alt: "Kate",
    },
  
    bob: {
      name: "Amos",
      age: "Age 14",
      review:
        "Julian has taught be both double bass and electric bass for 4 years and got me from having trouble holding the bass, to joining the top groups in my school and recording my own songs. He teaches more than just the instrument but how to compose music, how to adapt and improvise in realistic musical contexts. He does all of this through positive feedback and constructive criticism.",
      thumbnailSrc: "./images/image.jpg",
      avatarSrc: "./images/image.jpg",
      alt: "Amos",
    },
  
    mary: {
      name: "Stella",
      age: "Age 17",
      review:
        "Julian originally began teaching me how to play Double Bass approximately 12 months ago but I have recently switched to Bass Guitar lessons.  I have had Bass Guitar lessons previously with other teachers but find Julian’s teaching technique more beneficial to me.  Julian is very supportive, encouraging and accommodating during my lessons which are either face to face or via Zoom.  He is an excellent Bass teacher and goes above and beyond for his students.",
      thumbnailSrc: "./images/image.jpg",
      avatarSrc: "./images/image.jpg",
      alt: "Stella",
    },
  };
  
  const thumbnailsArray = [
    thumbnailsObject.hassan,
    thumbnailsObject.john,
    thumbnailsObject.bob,
    thumbnailsObject.mary,
  ];
  
  /////////////////////////////////////////////////////////////////////////////////
  
  const thumbnailContainer = document.querySelector(".thumbnail-container");
  
  /**
   * a function that makes the thumbnail container more dynamic, in the future if we need to add more reviewers we dont have to change the HTML but just add their details in the above object and Array and the function will add them to the layout.
   * @param {array} array - any array that contains objects with the above key value schema.
   */
  const addThumbnails = (array) => {
    for (let i = 0; i < array.length; i++) {
      thumbnailContainer.insertAdjacentHTML(
        "beforeend",
        `<img
      class="thumbnail"
      src= ${array[i].thumbnailSrc}
      alt=${array[i].alt}
      data-index=${i}
    />`
      );
    }
    thumbnailContainer.firstElementChild.classList.add("selected-thumbnail");
    thumbnailContainer.lastElementChild.classList.add("deselected-thumbnail");
  };
  
  addThumbnails(thumbnailsArray);
  
  /////////////////////////////////////////////////////////////////////////////////
  // THE BELOW ARE DOM ELEMENTS and VARIABLES WE NEED TO WORK ON OUR SECTION ://///
  /////////////////////////////////////////////////////////////////////////////////
  
  const avatar = document.querySelector(".avatar");
  const carousel = document.querySelector(".carousel-wrapper");
  const reviewerName = document.querySelector(".reviewer-name");
  const age = document.querySelector(".reveiwer-age");
  const text = document.querySelector(".reviewer-text");
  const thumbnailCollection = document.querySelectorAll(".thumbnail");
  const reviewerDataContainer = carousel.querySelector(".reviewer-data-wrapper");
  let currentAvatar = 0;
  let timeoutId;
  let selectedThumbnail;
  let deselectedThumbnail;
  /**
   * @param {number} index the argument which is used to select which image to show.
   
   * @description a function that sets the source and alt attributes of the avatar
   */
  
  const setAndShowAvatar = (index) => {
    avatar.setAttribute("src", `${thumbnailsArray[index].avatarSrc}`);
    avatar.setAttribute("alt", `${thumbnailsArray[index].alt}`);
  };
  
  setAndShowAvatar(currentAvatar);
  
  /**
   * @description a function that cycles between selected thumbnails
   * @param {number} index the argument which is used to select which thumbnail to show.
   */
  
  const selectThumbnail = (index) => {
    selectedThumbnail = thumbnailContainer.querySelector(".selected-thumbnail");
    deselectedThumbnail = thumbnailContainer.querySelector(
      ".deselected-thumbnail"
    );
  
    deselectedThumbnail.classList.remove("deselected-thumbnail");
    selectedThumbnail.classList.add("deselected-thumbnail");
    selectedThumbnail.classList.remove("selected-thumbnail");
    thumbnailCollection[index].classList.add("selected-thumbnail");
  };
  
  /**
   * @description a function that fills and shows the name, age, and review of each reviewer.
   * @param {number} index the argument which is used to select which text to show.
   */
  
  const changeReviewerContent = (index) => {
    reviewerName.innerText = `${thumbnailsArray[index].name}`;
    age.innerText = `${thumbnailsArray[index].age}`;
    text.innerText = `${thumbnailsArray[index].review}`;
  };
  
  changeReviewerContent(currentAvatar);
  
  //////////////////////////////////EVENT LISTENER/////////////////////////////////////
  
  carousel.addEventListener("click", (event) => {
    if (event.target.tagName == "BUTTON") {
      if (event.target.dataset.direction == "next") {
        currentAvatar += 1;
        if (currentAvatar >= thumbnailsArray.length) {
          currentAvatar = 0;
        }
      } else {
        currentAvatar -= 1;
        if (currentAvatar < 0) {
          currentAvatar = thumbnailsArray.length - 1;
        }
      }
    } else if (event.target.classList.contains("thumbnail")) {
      currentAvatar = +event.target.dataset.index;
    }
  
    clearTimeout(timeoutId);
    $(avatar).fadeOut(350);
    $(reviewerDataContainer).fadeOut(350);
  
    timeoutId = setTimeout(() => {
      setAndShowAvatar(currentAvatar);
      changeReviewerContent(currentAvatar);
      $(avatar).fadeIn(350);
      $(reviewerDataContainer).fadeIn(350);
    }, 350);
    selectThumbnail(currentAvatar);
  });
  